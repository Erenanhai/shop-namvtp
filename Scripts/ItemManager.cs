using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemManager : MonoBehaviour
{
    MoneyManager moneyManager;
    AudioManager audioManager;

    [SerializeField] private Item item;

    [SerializeField] private Button itemButton;

    public GameObject confirmWindow;
    [SerializeField] private TMP_Text confirmContent;
    [SerializeField] private Button confirmButton;
    //[SerializeField] private GameObject confirmItemSprite;

    [SerializeField] private TMP_Text nameDisplay;
    [SerializeField] private GameObject itemDisplay;
    [SerializeField] private TMP_Text priceDisplay;

    [SerializeField] private Sprite[] coinSprites;
    [SerializeField] private GameObject currencyType;
    [SerializeField] private string type;

    //public GameObject itemPrefab;

    private void Awake()
    {
        itemButton = GetComponent<Button>();
        moneyManager = FindObjectOfType<MoneyManager>();
        audioManager = FindObjectOfType<AudioManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        itemButton.interactable = true;
        item.sold = false;
        confirmWindow.SetActive(false);

        
        nameDisplay.text = item.itemName;
        itemDisplay.GetComponent<Image>().sprite = item.itemSprite;
        priceDisplay.text = item.price.ToString();

        switch (item.currentType)
        {
            case Item.CurrentType.Copper:
                currencyType.GetComponent<Image>().sprite = coinSprites[0];
                type = " coppers";
                break;
            case Item.CurrentType.Silver:
                currencyType.GetComponent<Image>().sprite = coinSprites[1];
                type = " silvers";
                break;
            case Item.CurrentType.Golden:
                currencyType.GetComponent<Image>().sprite = coinSprites[2];
                type = " golds";
                break;
        }
        

        //currencyType.GetComponent<Image>().sprite = item.currentType;
    }

    // Update is called once per frame
    void Update()
    {
        if (item.sold == true) 
        {
            itemButton.interactable = false;
            priceDisplay.text = "Sold!"; 
        }
    }

    public void Confirm()
    {
        audioManager.Play("Click");
        confirmWindow.SetActive(true);

        confirmButton.onClick.AddListener(delegate { moneyManager.Buy(item.price, item.currentType); audioManager.Play("Buy"); item.sold = true; });

        confirmContent.text = "Do you want to buy this " + item.itemName + " for " + item.price.ToString() + type + "?";
        //confirmItemSprite.GetComponent<RawImage>().texture = item.itemSprite.texture;
            
    }


}
