using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public MoneyManager moneyManager;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    
    // Start is called before the first frame update
    void Start()
    {
        moneyManager = GetComponent<MoneyManager>();

        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize()
    {
        moneyManager.copperCoin = 3000;
        moneyManager.silverCoin = 2000;
        moneyManager.goldenCoin = 1000;
    }


}
