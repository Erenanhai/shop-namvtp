using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MoneyManager : MonoBehaviour
{
    public static MoneyManager instance;

    public int copperCoin;
    public int silverCoin;
    public int goldenCoin;

    [SerializeField] private TMP_Text copperCoinDisplay;
    [SerializeField] private TMP_Text silverCoinDisplay;
    [SerializeField] private TMP_Text goldenCoinDisplay;


    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        copperCoinDisplay.text = copperCoin.ToString();
        silverCoinDisplay.text = silverCoin.ToString();
        goldenCoinDisplay.text = goldenCoin.ToString();
    }

    public void Buy(int price, Item.CurrentType type) 
    {
        switch (type)
        {
            case Item.CurrentType.Copper:
                copperCoin -= price;
                break;
            case Item.CurrentType.Silver:
                silverCoin -= price;
                break;
            case Item.CurrentType.Golden:
                goldenCoin -= price;
                break;
        }
    }
}
