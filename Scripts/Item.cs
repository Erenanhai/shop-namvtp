using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Render/Item")]
public class Item : ScriptableObject
{
    public string itemName;

    public int price;
    public enum CurrentType {Copper, Silver, Golden}
    public CurrentType currentType;

    public Sprite itemSprite;

    public bool sold;

    
}
